require 'rails_helper'

RSpec.feature "Users", type: :feature do
  let(:user) {FactoryGirl.create(:user)}
  describe "Login" do
  	it "login user" do
  	  sign_in(user.email, user.password)
  	  expect(page).to have_content "サンプルアプリ"
  	end

  	it "Logput" do
  	  sign_in(user.email, user.password)
  	  expect(page).to have_content "サンプルアプリ"
  	  click_link "ログアウト"
  	  expect(page).to have_content "Log in"
  	end

  	it "edit user infomation" do
  	  sign_in(user.email, user.password)
  	  click_link "編集"
  	  expect(page).to have_content "Edit User"
  	  fill_in "Password", with: "TestTest"
  	  fill_in "Password confirmation", with: "TestTest"
  	  fill_in "Current password", with: user.password
  	  click_button "Update"
  	  expect(page).to have_content "サンプルアプリ"
  	  click_link "ログアウト"
  	  sign_in(user.email, "TestTest")
  	  expect(page).to have_content "サンプルアプリ"
  	end
  end
end
