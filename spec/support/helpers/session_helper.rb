module Features

  module SessionHelper

    def extract_confirmation_url(mail)
      body = mail.body.encoded
      body[/http[^"]+/]
    end

    def sign_up_with(username, email, password)
      visit new_user_registration_path
      fill_in "Username", with: username
      fill_in "Email", with: email
      fill_in "Password", with: password
      click_button "Sign up"
      mail = ActionMailer::Base.deliveries.last
      url = extract_confirmation_url(mail)
      visit url
    end

    def sign_in(email, password)
      visit new_user_session_path
      fill_in "Email", with: email
      fill_in "Password", with: password
      click_button "Log in"
    end
  end
end
