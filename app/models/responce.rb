class Responce < ApplicationRecord
  belongs_to :user
  has_many :microposts, dependent: :destroy
  default_scope -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :title, presence: true, length: {maximum: 50}
end
