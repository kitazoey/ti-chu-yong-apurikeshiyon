Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'static_page#index'
  devise_for :users, controllers: {
    sessions: 			'users/sessions',
    passwords: 			'users/passwords',
    registrations: 		'users/registrations',
    confirmations: 		'users/confirmations',
    unlocks: 			'users/unlocks'
   }
    devise_scope :user do
      get '/users/sign_out' => 'devise/sessions#destroy'
    end
  resources :users, only:[:show]
  resources :responces, only:[:show, :create, :destroy]
  resources :microposts, only:[:create]
end
