class UsersController < ApplicationController

  def show
    @user = User.find(params[:id])
    @responces = @user.responces
    @thread = current_user.responces.build if user_signed_in?
  end

end
