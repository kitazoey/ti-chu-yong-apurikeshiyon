require 'support/helpers/session_helper'

RSpec.configure do |config|
  config.include Features::SessionHelper, type: :feature
end
