FactoryGirl.define do
  factory :user do
  	username "Alice"
  	email "example@example.com"
  	password "example"

  	before(:create){ |user|
      # ここで認証済みでメールを送信しない設定を行う
      user.skip_confirmation_notification!
      user.skip_confirmation!

    }
  end
end