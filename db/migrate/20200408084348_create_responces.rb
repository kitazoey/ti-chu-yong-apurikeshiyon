class CreateResponces < ActiveRecord::Migration[5.1]
  def change
    create_table :responces do |t|
      t.text :title
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :responces, [:user_id, :created_at]
  end
end
