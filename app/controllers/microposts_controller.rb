class MicropostsController < ApplicationController

  def create
  	@micropost = current_user.microposts.build(micropost_params)
  	if @micropost.save
  	  flash[:success] = "Comment created"
  	  redirect_to responce_path(@micropost.responce_id)
  	else
  	  render user_path(current_user.id)
  	end
  end

  private

    def micropost_params
      params.require(:micropost).permit(:context, :responce_id)
    end
end
