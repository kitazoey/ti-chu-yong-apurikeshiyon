class ResponcesController < ApplicationController
  before_action :correct_user, only: :destroy

  def show
  	@responce = Responce.find(params[:id])
  	@microposts = @responce.microposts
  	@comment = current_user.microposts.build if user_signed_in?
  end

  def create
  	@responce = current_user.responces.build(responce_params)
  	if @responce.save
  	  flash[:success] = "Thread created"
  	  redirect_to responce_path(@responce.id)
  	else
  	  render user_path(current_user.id)
  	end
  end

  def destroy
  	@responce.destroy
  	flash[:success] = "Thread destroy"
  	redirect_to user_path(current_user.id)
  end

  private

    def responce_params
      params.require(:responce).permit(:title)
    end

    def correct_user
      @responce = current_user.responces.find_by(id: params[:id])
      redirect_to root_path if @responce.nil?
    end
end
