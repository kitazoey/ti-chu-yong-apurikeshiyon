class AddResponceIdToMicroposts < ActiveRecord::Migration[5.1]
  def change
    add_column :microposts, :responce_id, :integer
    add_index :microposts, [:responce_id, :created_at]
  end
end
